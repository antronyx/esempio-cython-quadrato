#ifndef FRECTANGLE_H
#define FRECTANGLE_H



    class fRectangle {
        public:
            int x0, y0, x1, y1;
            //fRectangle();
            fRectangle(int x0, int y0, int x1, int y1);
            //~fRectangle();
            int getArea();
            void getSize(int* width, int* height);
            void move(int dx, int dy);
	protected:

    };

#endif
