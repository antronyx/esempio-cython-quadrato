from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext


setup(
    version="0.01",
    author="Antonino Cacicia",
    author_email="antoniocacicia1997@gmail.com",
    cmdclass = {'build_ext': build_ext},
    ext_modules = [
        Extension("rect",
            language="c++",
            sources=["rect.pyx"],
            #include_dirs=["/home/share/data/code/python/test/include"],
            #libraries = ["TestUPtr"],
            #library_dirs=["/home/share/data/code/python/test/lib"],
            extra_compile_args=['--no-undefined','-lstdc++','-std=c++11', '-v'],
            extra_link_args=['-lstdc++', '-v'],
            )
        ]
    )
